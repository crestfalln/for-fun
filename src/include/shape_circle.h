#ifndef SHAPE_CIRCLE_H
#define SHAPE_CIRCLE_H

#include "shape_interface.h"

namespace utils
{
template <typename _T> struct GenericCircle : public GenericShape<_T>
{
  using Point = GenericPoint<_T>;
  Point                               m_center;
  _T                                  m_r;
  _T                                  m_area;
  std::vector<Point>                  m_verticies;
  std::pair<Point, std::pair<_T, _T>> m_bounds;
  GenericCircle(Point const &center, _T r)
      : m_center(center), m_r(r),
        m_bounds({{center.i() - r, center.j() - r}, {2 * r, 2 * r}}),
        m_verticies()
  {
    m_verticies.shrink_to_fit();
  }

  virtual bool testPoint(Point const &point) const override
  {
    return distanceAbs(point, getCenter()) <= square(getRadius());
  }
  virtual Point getCenter() const override { return m_center; }
  virtual _T    getRadius() const override { return m_r; }
  virtual std::pair<Point, std::pair<_T, _T>> getBounds() const override
  {
    return m_bounds;
  };
  virtual Point getFurthest() const override
  {
    return {getCenter().i() + getRadius(), getCenter().j()};
  }
  virtual _T    getRadius() override { return m_r; }
  virtual Point getFurthest() override
  {
    return {getCenter().i() + getRadius(), getCenter().j()};
  }
  virtual std::vector<Point> const &getVerticies() const override
  {
    return m_verticies;
  }
  virtual _T    getArea() const override { return m_area; }
  virtual int   countVertices() const override { return -1; }
  virtual Point move(Vector<_T> const &shift) override
  {
    m_center = m_center + shift;
    return m_center;
  }
};

template <typename _T> struct GenericEllipse : public GenericCircle<_T>
{
  _T r2;
};

using Circle  = GenericCircle<int>;
using Ellipse = GenericEllipse<int>;
}; // namespace utils

#endif