#ifndef TIMER_H
#define TIMER_H

#include <chrono>
#include <ratio>
#include <stdexcept>
#include <thread>

namespace utils
{
class Timer
{
private:
  std::chrono::steady_clock clock;

  Timer() = default;

public:
  using us            = std::chrono::microseconds;
  using ms            = std::chrono::milliseconds;
  using ns            = std::chrono::nanoseconds;
  using s             = std::chrono::seconds;
  using duration_type = us;
  using time_point_type =
      std::chrono::time_point<std::chrono::steady_clock, duration_type>;

  static Timer &getInstance();
  static auto   now() -> time_point_type;
  Timer(Timer const &) = delete;
  void operator=(Timer const &) = delete;
};

template <typename _Duration> inline void wait(_Duration const &duration)
{
  std::this_thread::sleep_for(duration);
}
} // namespace utils

#endif