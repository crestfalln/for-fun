#ifndef INPUT_H
#define INPUT_H

#include <SDL2/SDL.h>
#include <map>

// Records keypresses and events
class inputs
{
    std::map<SDL_Scancode, bool> m_held_keys;
    std::map<SDL_Scancode, bool> m_pressed_keys;
    std::map<SDL_Scancode, bool> m_released_keys;

public:
    void newFrame();

    bool handleKeyEvent(SDL_Event const&);
private:
    void keyUp(SDL_Event const&);
    void keyDown(SDL_Event const&);

    bool wasPressed(SDL_Scancode);
    bool wasReleased(SDL_Scancode);
    bool isHeld(SDL_Scancode);
};

#endif