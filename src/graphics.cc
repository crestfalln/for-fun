#include "graphics.h"
#include "SDL_render.h"
#include "SDL_surface.h"
#include "SDL_video.h"
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_stdinc.h>
#include <cstdint>
#include <memory>
#include <stdexcept>
#include <string>
#ifdef DEBUG_BENCH
#include "timer_debug.h"
#endif

SDL_Texture *Graphics::makeTextureFromShape(utils::Shape const &shape)
{
  auto width     = shape.getBounds().second.first;
  auto height    = shape.getBounds().second.second;
  auto bindPoint = shape.getBounds().first;
  auto set_pixel = [](SDL_Surface *surface, int x, int y, Uint32 pixel)
  {
    Uint32 *const target_pixel =
        (Uint32 *)((Uint8 *)surface->pixels + y * surface->pitch +
                   x * surface->format->BytesPerPixel);
    *target_pixel = pixel;
  };

  SDL_Surface *tempSurface = SDL_CreateRGBSurfaceWithFormat(
      0, width, height, 32, SDL_PIXELFORMAT_ARGB8888);
  SDL_LockSurface(tempSurface);
#ifdef DEBUG_BENCH
  {
    StopWatch stp("My Fill Internal: ");
#endif
    for (int j = 0; j < height; ++j)
    {
      for (int i = 0; i < width; ++i)
      {
        if (shape.testPoint({bindPoint.i() + i, bindPoint.j() + j}))
          set_pixel(tempSurface, i, j,
                    SDL_MapRGBA(tempSurface->format, 0, 255, 0, 100));
      }
    }
#ifdef DEBUG_BENCH
  }
#endif
  SDL_UnlockSurface(tempSurface);
  SDL_Texture *tex = SDL_CreateTextureFromSurface(m_renderer, tempSurface);
  SDL_FreeSurface(tempSurface);
  return tex;
}

/*
 * Graphics Impl
 */
Graphics::Graphics(std::string const &title, std::pair<int, int> const &res,
                   std::pair<int, int> const &pos)
{
  if (pos.first == -1 || pos.second == -1)
    m_window = SDL_CreateWindow(title.c_str(), SDL_WINDOWPOS_UNDEFINED,
                                SDL_WINDOWPOS_UNDEFINED, res.first, res.second,
                                SDL_WINDOW_SHOWN);
  if (m_window == nullptr)
    throw std::runtime_error("Error while creating window: " +
                             static_cast<std::string>(SDL_GetError()));
  m_renderer = SDL_CreateRenderer(m_window, -1, SDL_RENDERER_ACCELERATED);
  if (m_renderer == nullptr)
    throw std::runtime_error("Error while creating rendererer: " +
                             static_cast<std::string>(SDL_GetError()));
}
Graphics::Graphics(Graphics &&graphics)
{
  m_renderer          = graphics.m_renderer;
  m_window            = graphics.m_window;
  graphics.m_renderer = nullptr;
  graphics.m_window   = nullptr;
}
Graphics::~Graphics()
{
  SDL_DestroyRenderer(m_renderer);
  SDL_DestroyWindow(m_window);
}

/*
 * Image Impl
 */
Image::Image(std::string const &path, Graphics &graphics)
    : m_path(path), m_graphics(graphics)
{
  load();
}
Image::Image(SDL_Texture *tex, Graphics &graphics)
    : m_path(""), m_graphics(graphics)
{
  m_tex = tex;
  tex   = nullptr;
}
Image::Image(Image &&img) noexcept : m_graphics(img.m_graphics)
{
  m_path    = img.m_path;
  m_tex     = img.m_tex;
  img.m_tex = nullptr;
  img.m_path.clear();
}
void Image::load()
{
  if (m_path.length() == 0)
    return;
  auto tmpSurface = IMG_Load(m_path.c_str());
  if (tmpSurface == nullptr)
    throw std::runtime_error("problem with loading asset: " +
                             static_cast<std::string>(IMG_GetError()));
  m_tex = SDL_CreateTextureFromSurface(m_graphics.m_renderer, tmpSurface);
  if (m_tex == nullptr)
    throw std::runtime_error("problem with loading asset: " +
                             static_cast<std::string>(IMG_GetError()));
  SDL_FreeSurface(tmpSurface);
}
void Image::render()
{
  SDL_RenderCopy(m_graphics.m_renderer, getTexture(), nullptr, nullptr);
}
Graphics &Image::getGraphics() { return m_graphics; }

SDL_Texture *Image::getTexture()
{
  if (m_tex == nullptr)
    load();
  return m_tex;
}
Image::~Image()
{
  if (m_tex != nullptr)
    SDL_DestroyTexture(m_tex);
}

Animation::Animation(ImageArray &&               assets,
                     utils::Timer::duration_type animation_time)
    : m_assets(std::move(assets)), m_animation_time(animation_time),
      m_graphics(m_assets[0].getGraphics())
{
  m_minDelta = m_animation_time / m_assets.size();
}

void Animation::renderCurrent()
{
  if (m_assets.size() == 0)
    return;
  m_assets[m_frame_no].render();
}

void Animation::renderNext()
{
  if (m_assets.size() == 0)
    return;
  if (m_frame_no + 1 == m_assets.size())
    m_frame_no = -1;
  m_assets[++m_frame_no].render();
}

void Animation::render(utils::Timer::duration_type frameTime)
{
  if (m_accumulator >= getMinFrameTime())
  {
    renderNext();
    m_accumulator = static_cast<utils::Timer::duration_type>(0);
  }
  else
  {
    m_accumulator += frameTime;
  }
}

void Animation::resetFrameCounter()
{
  m_frame_no    = 0;
  m_accumulator = static_cast<utils::Timer::duration_type>(0);
}

auto Animation::getMinFrameTime() -> utils::Timer::duration_type
{
  return m_minDelta;
}