#include "timer_debug.h"
StopWatch::StopWatch(std::string const &msg) : m_msg(msg)
{
  start = clock.now();
}
StopWatch::~StopWatch()
{
  stop      = clock.now();
  auto time = static_cast<long>((stop - start).count());
  fprintf(stderr, "%s%fms\n", m_msg.c_str(), time / 1000000.f);
  fflush(stderr);
}